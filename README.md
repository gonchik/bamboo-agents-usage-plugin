# README #

Plugin for Bamboo which shows builds in timeline view. 
Allows to see utilization of agents and server load. 
![agents_usage.png](https://bitbucket.org/repo/Bnj54A/images/3231498354-agents_usage.png)
# Installation #
Plugin can be installed from [Marketplace](https://marketplace.atlassian.com/plugins/com.atlassianlab.bamboo.plugins.bamboo-agents-usage/server/overview) or built from sources.
To build plugin, checkout repository and then execute
```
mvn clean package
```
then use Bamboo > Administration > Manage Add-ons > Upload addon and upload jar file from target folder

# Licenses #
d3 (https://d3js.org) - BSD License
d3-timeline (https://github.com/jiahuang/d3-timeline) - MIT license

Legal information
=================

To contribute if you are an Atlassian customer no further action is required because our
[End User Agreement](http://www.atlassian.com/end-user-agreement/) (Section 7) gives Atlassian the right to use
contributions from customers. 
If your are not an Atlassian customer then you will need to sign and submit our [Contribution Agreement](ACLA.pdf).


How to contribute
=================
The source code is stored in a Git repository on Bitbucket. To checkout it run:

    git clone https://bitbucket.org/atlassianlabs/bamboo-agents-usage-plugin.git

To contribute you code to the plugin please:
* create a feature branch, 
* change the code, 
* add unit and/or integration tests, 
* test it and 
* eventually create a [pull request](https://bitbucket.org/atlassianlabs/bamboo-agents-usage-plugin/pull-requests)
to review them. 
After our review, your feature branch will be merged into the master branch!

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

See _Contributors License Agreement_ section of [Open Source at Atlassian](https://developer.atlassian.com/opensource/) page.

### Who do I talk to? ###

* Alexey Chystoprudov, achystoprudov@atlassian.com

## License ##

    Copyright (c) 2016, Atlassian Pty. Ltd.
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    The names of contributors may not
    be used to endorse or promote products derived from this software without
    specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.