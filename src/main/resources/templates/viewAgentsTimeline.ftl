<html>
<head>
    <title>
        Agents utilization
    </title>
    <meta name="decorator" content="adminpage"/>
    <meta name="tab" content="agents-timeline" />
</head>
<body>
    <h1>Agents usage</h1>
    <div class="agent-usage-controls">
        <label for="input-filter">Agent Filter</label>
        <input id="input-filter" class="text" type="text" name="input-filter" value="" />
        <input id="start-range" class="aui-date-picker text" type="datetime-local" min="2006-12-21" value=""/> <#--Bamboo first release-->
        <input id="end-range" class="aui-date-picker text" type="datetime-local" />
        <input id="updateBtn" type="button" class="aui-button aui-button-primary" value="Update">
    </div>
    <div id="agents-usage-content" style="width:100%"></div>
    <script type="text/javascript">
        require(['plugin/agents-usage/agents-usage-app', 'jquery'], function(AgentsUsage, $) {
            new AgentsUsage({
                el:'#agents-usage-content',
                inputFilter: "#input-filter",
                startDatePicker: '#start-range',
                endDatePicker: '#end-range',
                updateButton: '#updateBtn',
                width: $('#agents-usage-content').width()
            });
        });
    </script>
</body>
</html>