package com.atlassianlabs.bamboo.plugins.agentsusage;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(FIELD)
public class AgentStatistics
{
    @XmlElement
    private final String agentName;
    @XmlElement
    private final List<BuildTime> times;

    public AgentStatistics(String agentName, List<BuildTime> times)
    {
        this.agentName = agentName;
        this.times = times;
    }

    public String getAgentName()
    {
        return agentName;
    }

    public List<BuildTime> getTimes()
    {
        return times;
    }
}
