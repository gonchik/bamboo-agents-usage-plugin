package com.atlassianlabs.bamboo.plugins.agentsusage;

import com.atlassian.bamboo.template.TemplateRenderer;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class AgentsUsageServlet extends HttpServlet
{
    private static final Logger log = Logger.getLogger(AgentsUsageServlet.class);

    private static final String TEMPLATE_PATH = "/templates/viewAgentsTimeline.ftl";

    private final TemplateRenderer templateRenderer;

    public AgentsUsageServlet(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html;charset=utf-8");
        try
        {
            String response = templateRenderer.renderWithoutActionContext(TEMPLATE_PATH, new HashMap<>());
            resp.getWriter().write(response);
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }
}
