package com.atlassianlabs.bamboo.plugins.agentsusage;

import com.atlassian.bamboo.agent.AgentType;
import com.atlassian.bamboo.agent.elastic.server.ElasticImageConfiguration;
import com.atlassian.bamboo.buildqueue.ElasticAgentDefinition;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryCriteria;
import com.atlassian.bamboo.resultsummary.ResultsSummaryCriteriaBuilder;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.sun.jersey.spi.resource.Singleton;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Path("agentsusage")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Singleton
@Scanned
public class AgentsUsageResource
{
    private static final Logger log = Logger.getLogger(AgentsUsageResource.class);
    private static final String DATE_TIME_FORMAT = "yyyy_MM_dd_HH_mm";

    private final ResultsSummaryManager resultsSummaryManager;
    private final AgentManager agentManager;

    private final LoadingCache<Long, String> agentCache = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .softValues()
            .build(new CacheLoader<Long, String>()
            {
                @Override
                public String load(@NotNull Long buildAgentId) throws Exception
                {
                    final BuildAgent agent = agentManager.getAgent(buildAgentId);
                    if (agent == null)
                    {
                        return "Unknown";
                    }

                    if (agent.getType() == AgentType.ELASTIC)
                    {
                        final ElasticImageConfiguration elasticImageConfiguration = ((ElasticAgentDefinition) agent.getDefinition()).getElasticImageConfiguration();
                        return elasticImageConfiguration.getConfigurationName() + " (" + elasticImageConfiguration.getAmiId() + ")";
                    }
                    return agent.getName();
                }
            });

    public AgentsUsageResource(@ComponentImport ResultsSummaryManager resultsSummaryManager,
                               @ComponentImport AgentManager agentManager)
    {
        this.resultsSummaryManager = resultsSummaryManager;
        this.agentManager = agentManager;
    }

    @GET
    @NotNull
    public List<AgentStatistics> getData(@Nullable @QueryParam("inputFilter") String inputFilter,
                                         @Nullable @QueryParam("from") String fromDate,
                                         @Nullable @QueryParam("to") String toDate) throws ExecutionException
    {
        final SimpleDateFormat format = new SimpleDateFormat(DATE_TIME_FORMAT);
        final ResultsSummaryCriteriaBuilder resultsSummaryCriteriaBuilder = new ResultsSummaryCriteriaBuilder()
                .setResultSummaryClass(BuildResultsSummary.class);
        setStartDate(fromDate, format, resultsSummaryCriteriaBuilder);
        setEndDate(toDate, format, resultsSummaryCriteriaBuilder);

        final ResultsSummaryCriteria criteria = resultsSummaryCriteriaBuilder.buildCriteria();
        final List<ResultsSummary> resultSummaries = resultsSummaryManager.getResultSummaries(criteria);
        final Map<String, List<BuildTime>> buildsByAgent = new TreeMap<>();
        for (ResultsSummary resultSummary : resultSummaries)
        {
            final String buildAgentName = Optional.ofNullable(resultSummary.getBuildAgentId())
                    .map(key ->
                    {
                        try
                        {
                            return agentCache.get(key);
                        }
                        catch (ExecutionException e)
                        {
                            return null;
                        }
                    })
                    .orElse("Unknown");
            List<BuildTime> buildTimes = buildsByAgent.get(buildAgentName);
            if (buildTimes == null)
            {
                buildTimes = new ArrayList<>();
                buildsByAgent.put(buildAgentName, buildTimes);
            }

            buildTimes.add(new BuildTime(resultSummary.getPlanResultKey().toString(), resultSummary.getStatDate(), resultSummary.getBuildCompletedDate()));
        }

        final List<AgentStatistics> result = new ArrayList<>();
        for (Map.Entry<String, List<BuildTime>> agentEntry : buildsByAgent.entrySet())
        {
            final String agentName = agentEntry.getKey();
            boolean includeAgent = true;
            if (isNotBlank(inputFilter))
            {
                if (!agentName.toLowerCase().contains(inputFilter.toLowerCase()))
                {
                    includeAgent = false;
                }
            }
            if (includeAgent)
                result.add(new AgentStatistics(agentName, agentEntry.getValue()));
        }
        return result;
    }

    private void setEndDate(@Nullable @QueryParam("to") String toDate, SimpleDateFormat format, ResultsSummaryCriteriaBuilder resultsSummaryCriteriaBuilder)
    {
        if (isNotBlank(toDate))
        {
            try
            {
                resultsSummaryCriteriaBuilder.setToDate(format.parse(toDate));
            }
            catch (ParseException e)
            {
                log.error(e.getMessage(), e);
            }
        }
        else
        {
            resultsSummaryCriteriaBuilder.setToDate(new Date());
        }
    }

    private void setStartDate(@Nullable @QueryParam("from") String fromDate, SimpleDateFormat format, ResultsSummaryCriteriaBuilder resultsSummaryCriteriaBuilder)
    {
        if (isNotBlank(fromDate))
        {
            try
            {
                resultsSummaryCriteriaBuilder.setFromDate(format.parse(fromDate));
            }
            catch (ParseException e)
            {
                log.error(e.getMessage(), e);
            }
        }
        else
        {
            resultsSummaryCriteriaBuilder.setFromDate(DateUtils.addHours(new Date(), -48));
        }
    }

}
