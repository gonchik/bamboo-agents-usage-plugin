package com.atlassianlabs.bamboo.plugins.agentsusage;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.Objects;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(FIELD)
public class BuildTime
{
    @XmlElement
    private final String key;
    @XmlElement
    private final Date startTime;
    @XmlElement
    private final Date endTime;

    public BuildTime(final String key, final Date startTime, final Date endTime)
    {
        this.key = key;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public Date getEndTime()
    {
        return endTime;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final BuildTime buildTime = (BuildTime) o;
        return Objects.equals(key, buildTime.key);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(key);
    }
}
